# Building the Documentation

1. Build `palisade_pke.js` and `palisade_binfhe.js`
2. Run `npm run build-ts-docs`

The files should appear in `docs-ts/`. Host a HTTP server there to browse.
