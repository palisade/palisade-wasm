import {doc} from './doc.mjs'

doc('Ciphertext_DCRTPoly', [], `
/** A ciphertext describes an encrypted message that you can perform
 *  homomorphic computations over, using a CryptoContext */ `)

doc('GetEncodingType', [], `
/**
* GetEncodingType
* @return how the Plaintext that this CiphertextImpl was created from was
* encoded
*/ `)
