import {doc} from './doc.mjs'

doc('BigInteger', [], `
/** Represents large integers stored in C++. */
` )

doc('DividedBy', ['b'], `
/**
* Division operation.
*
* @param &b is the value to divide by.
* @return is the result of the division operation.
*/ `)

doc('ConvertToDouble', [], `
/**
* Converts the value to an double.
*
* @return double representation of the value.
*/ `)
