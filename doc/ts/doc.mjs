
export const annotations = {}
export function doc(name,parameterNames,comment,types) {
	types = types || {}
    annotations[name] = {
        comment: comment.trim(),
        parameters:parameterNames
			.map(
				name => ({name, typename: types[name]})
			)
    }
}
export const annotate = specifier => {
    const annotation = annotations[specifier.name]

	if (!annotation) {
		console.warn(`No documentation for ${specifier.name}`)
	}

	return annotation
}
