import fs from 'fs'
import {generateTypescriptBindings} from 'tsembind'
import {doc,annotate,annotations} from './doc.mjs'

import './cryptocontext.mjs'
import './plaintext.mjs'
import './biginteger.mjs'
import './ciphertext.mjs'

async function generateDeclarationFiles() {
	let bindings;
	annotations["CustomEmbindModule"] = { name: "PKEModule" }
	bindings =
		await generateTypescriptBindings('./lib/palisade_pke.js', annotate)
	fs.writeFileSync('./lib/palisade_pke.d.ts',bindings)

	annotations["CustomEmbindModule"] = { name: "BinfheModule" }
	// don't export serialization type twice (shared by binfhe and pke)
	annotations["SerType"] = { shouldExport: false }
	bindings =
		await generateTypescriptBindings('./lib/palisade_binfhe.js', annotate)
	fs.writeFileSync('./lib/palisade_binfhe.d.ts',bindings)
}
generateDeclarationFiles()
