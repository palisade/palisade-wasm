## Palisade Crypto (WebAssembly)

This documentation describes the WebAssembly port of the
Palisade homomorphic encryption library from the perspective of
a JavaScript/Typescript developer.

Most functionality is exposed through a
[CryptoContext](classes/cryptocontext_dcrtpoly.html) instance.

A CryptoContext can be generated through any of the following methods.
Some schemes are not yet optimized for WebAssembly.

- [GenCryptoContextBFV](interfaces/pkemodule.html#gencryptocontextbfvrns.html) (NOT optimized)
- [GenCryptoContextBGV](interfaces/pkemodule.html#gencryptocontextbgvrns.html) (optimized)
- [GenCryptoContextCKKS](interfaces/pkemodule.html#gencryptocontextckks.html) (optimized)

If you're looking to get started,the
[examples](https://gitlab.com/palisade/palisade-wasm/-/tree/master/examples)
may be a useful resource.
