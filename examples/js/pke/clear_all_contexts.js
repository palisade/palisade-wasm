/**
 * Step 1:
 * 	Generate cryptocontext, c_old
 * Step 2:
 * 	Serialize c_old to a buffer
 * Step 3:
 * 	Clear the old cc
 * Step 4:
 * 	Run the rest of the script (keygen, and math operations on ciphertexts)
 * @returns {Promise<number>}
 */

const DATAFOLDER = "demoData";

async function main() {
	const factory = require('../../../lib/palisade_pke')
	const module = await factory();

	console.log(
		`This program requires the subdirectory \`${DATAFOLDER} ` +
		`to exist, otherwise you will get an error writing serializations.`
	);

	// Sample Program: Step 1: Set CryptoContext

	// Set the main parameters
	const plaintextModulus = 65537;
	const sigma = 3.2;
	const securityLevel = module.SecurityLevel.HEStd_128_classic;
	const depth = 2;

	// Instantiate the crypto context
	const cryptoContext = new module.GenCryptoContextBFVrns(
		plaintextModulus, securityLevel, sigma, 0, depth, 0,
		module.MODE.OPTIMIZED
	);
	// Enable features that you wish to use
	cryptoContext.Enable(module.PKESchemeFeature.ENCRYPTION);
	cryptoContext.Enable(module.PKESchemeFeature.SHE);

	console.log("The cryptocontext has been generated.");

	// Serialize cryptocontext
	const cryptoContextBuffer =
		module.SerializeCryptoContextToBuffer(cryptoContext,module.SerType.BINARY);
	console.log("The cryptocontext has been serialized");

	module.ReleaseAllContexts()
	console.log("The old cryptocontext has been cleared")

	/**
	 * Do normal H.E operations
	 */
	const cc = module.DeserializeCryptoContextFromBuffer(
		cryptoContextBuffer, module.SerType.BINARY);


	const keyPair = cc.KeyGen();
	cc.EvalMultKeyGen(keyPair.secretKey);
	cc.EvalSumKeyGen(keyPair.secretKey);
	console.log("All the keys have been generated for the new Cryptocontext")

	// Sample Program: Step 3: Encryption

	const vectorOfInts1 =
		module.MakeVectorInt64Clipped([1,2,3,4,5,6,7,8,9,10,11,12]);
	const plaintext1 = cc.MakePackedPlaintext(vectorOfInts1);

	console.log(`Plaintext #1: ${plaintext1}`)

	// The encoded vectors are encrypted
	const ciphertext1 = cc.Encrypt(keyPair.publicKey,plaintext1);
	console.log("The plaintexts have been encrypted");

	// Homomorphic additions
	const ciphertextAdd = cc.EvalAddCipherCipher(ciphertext1, ciphertext1);
	// Homomorphic multiplications
	const ciphertextMul = cc.EvalMultCipherCipher(ciphertext1, ciphertext1);
	// Homomorphic rotations

	// Homomorphic summation
	const ciphertextSum = cc.EvalSum(ciphertext1,cc.GetBatchSize());

	const sk = keyPair.secretKey;
	// Decrypt the result of additions
	const plaintextAddResult = cc.Decrypt(sk, ciphertextAdd);
	plaintextAddResult.SetLength(vectorOfInts1.size());
	// Decrypt the result of multiplications
	const plaintextMultResult = cc.Decrypt(sk, ciphertextMul);
	plaintextMultResult.SetLength(vectorOfInts1.size());
	// Decrypt the result of rotations

	// Decrypt the result of sumation
	const plaintextSum= cc.Decrypt(sk, ciphertextSum);
	plaintextSum.SetLength(vectorOfInts1.size());

	// Output results
	console.log("\nResults of homomorphic computations");
	console.log(`#1 + #2 + #3: ${plaintextAddResult}`);
	console.log(`#1 * #2 * #3: ${plaintextMultResult}`);
	console.log(`Summation of all elements in #1: ${plaintextSum}`);

	return 0;
}
main().then(exitCode=>console.log(exitCode));
