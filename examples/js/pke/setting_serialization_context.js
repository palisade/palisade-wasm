async function main() {
  const factory = require('../../../lib/palisade_pke')
  const module = await factory()
  // Set the main parameters
  // all int types are number in typescript unless defined otherwise.
  const plaintextModulus = 65537;
  const sigma = 3.2;
  const depth = 2;
  // Instantiate the crypto context (CryptoContext type doesn't exist yet)
  // from inttypes.h, MODE: enum =  { RLWE= 0, OPTIMIZED =1, SPARSE =2};
  // This should have a custom type (not any) when cryptoContext type is
  // defined.
  cryptoContext = new module.GenCryptoContextBFVrns(
      plaintextModulus, module.SecurityLevel.HEStd_128_classic, sigma, 0, depth, 0, module.MODE.OPTIMIZED);

  let originalFlagVal = module.GetContextSer();
  console.log(`Truthiness was ${originalFlagVal}`);
  module.SetContextSer(false);
  console.log(`Setting Context Ser to ${originalFlagVal}`);

  let newFlagVal = module.GetContextSer();
  console.log(`Truthiness was ${newFlagVal}`);

  return 0;
}
main().then(exitCode => console.log(exitCode));