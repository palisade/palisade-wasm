#ifndef _PALISADEWEB_CORE_VERSION_H_
#define _PALISADEWEB_CORE_VERSION_H_

#include <emscripten.h>
#include <emscripten/bind.h>

using namespace emscripten;

EMSCRIPTEN_BINDINGS(palisade_version) {
	emscripten::function("GetPALISADEVersion", &GetPALISADEVersion);
}

#endif
