#ifndef _PALISADEWEB_CORE_PRINTH_
#define _PALISADEWEB_CORE_PRINTH_


/**
 * @brief Get string value from an PALISADE object.
 * @param object - PALISADE supported object.
 * @return string to be p.
 */
template <typename Element>
std::string GetString(const Element &object) {
  std::stringstream ss;
  ss << object;
  return ss.str();
}

#endif
